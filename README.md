## Description

- Slack-ratio-bot is a product price comparison slack chat bot
- It uses python web scraper to fetch the product prices
- The application provides the lowest price possible in both amazon and flipkart websites
- User can interact through Slack-ratio-bot (slack channel) 

## Dependency Installation Steps

- pip3 install -r requirement.txt

## To Run the Application

- git clone https://gitlab.com/bharathofficial1903/slack-ratio-bot.git
- python main.py

## SLACK Slash command usage

- /find_amazon = Search all product from amazon based on the input and result will be published in the Slack channel
- /find_amazon oneplus
![Screenshot_2022-02-24_at_8.51.14_PM](/uploads/a39034560ccb45570b05402356db80c1/Screenshot_2022-02-24_at_8.51.14_PM.png)


- /find_flipkart = Search all product from flipkart based on the input and result will be published in the Slack channel
- /find_flipkart oneplus
![Screenshot_2022-02-24_at_9.01.21_PM](/uploads/412e85aaa1337b41590cb0ab94da3c58/Screenshot_2022-02-24_at_9.01.21_PM.png)
