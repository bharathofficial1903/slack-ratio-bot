import slack
import os
from flask import Flask, request, Response
from dotenv import load_dotenv
from slackeventsapi import SlackEventAdapter
load_dotenv()
import scrap

app = Flask(__name__)
slack_event_adapter=SlackEventAdapter(os.environ.get('SLACK_SECRET'),'/',app)
client = slack.WebClient(token=os.environ['SLACK_TOKEN'])

@slack_event_adapter.on('app_home_opened')
def app_home_opened(payload):
        event = payload.get('event', {})
        user_id = event["user"]
        client.views_publish(
            user_id=user_id,
            view={
                "type": "home",
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": ":dizzy: *Welcome to Slack-Ratio-Bot, <@{}> Enjoy Shopping :tada:* ".format(user_id),
                        },
                    },
                    {
			            "type": "section",
			            "text": {
				        "type": "mrkdwn",
				        "text": "Please follow the slash commands"
			            }
                    },
                    {"type": "divider"},
                    {
			            "type": "section",
			            "text": {
				        "type": "mrkdwn",
				        "text": "`/find_amazon <product_name> `: Search the lowest possible price for your product from amazon "
			            }
                    },
                    {
			            "type": "section",
			            "text": {
				        "type": "mrkdwn",
				        "text": "`/find_flipkart <product_name> `: Search the lowest possible price for your product from flipkart <br />"
			            }
                    },
                    {"type": "divider"},
                    {
			            "type": "section",
			            "text": {
				        "type": "mrkdwn",
				        "text": ":orange_book: *NOTE*: Please don't forget to add  `<space>`  between words in the  `<product_name>`  as well"
			            }
                    }
                ],
            })


@app.route('/find_amazon', methods=['POST'])
def find_amazon():
    if request.method == 'POST':
        data = request.form
        channel_id = data.get('channel_id')
        user_id = data.get('user_id')
        client.chat_postMessage(
                channel=channel_id, text="Results Getting Ready....")
        scrap.search_amazon(data.get('text'),user_id)
    else:
        print("Please use POST Method")

    return 'success',200

@app.route('/find_flipkart', methods=['POST'])
def find_flipkart():
    if request.method == 'POST':
        data = request.form
        user_id = data.get('user_id')
        channel_id = data.get('channel_id')
        client.chat_postMessage(
                channel=channel_id, text="Results Getting Ready....")
        scrap.search_flipkart(data.get('text'),user_id)
    else:
        print("Please use POST Method")

    return 'success',200

@app.route('/compare', methods=['POST'])
def compare():
    return 'Comparing'

@app.route('/', methods=['GET'])
def test():
    return Response('Work Check!')
    
if __name__ == "__main__":
    app.debug = True
    app.run(host="0.0.0.0")
