import requests
import os
from dotenv import load_dotenv
load_dotenv()

slack_url = os.environ.get('SLACK_URL')
slack_token=os.environ.get('SLACK_TOKEN')

def send_msg(message,user):
    data1 = {
    'token': slack_token,
    'channel': user,
    'as_user': True,
    'text': message
    }
    res=requests.post(url=slack_url,data=data1)
    print(res)
