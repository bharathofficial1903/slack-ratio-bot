import requests
from bs4 import BeautifulSoup
import slack_bot

amazon_headers = {
            'authority': 'www.amazon.in',
            'pragma': 'no-cache',
            'cache-control': 'no-cache',
            'dnt': '1',
            'upgrade-insecure-requests': '1',
            'user-agent': 'Mozilla/5.0 (X11; CrOS x86_64 8172.45.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.64 Safari/537.36',
            'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'sec-fetch-site': 'none',
            'sec-fetch-mode': 'navigate',
            'sec-fetch-dest': 'document',
            'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
}
flipkart_headers={
    'Host': 'www.flipkart.com',
    'Connection': 'keep-alive',
    'Cache-Control': 'max-age=0',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/98.0.4758.102 Safari/537.36',
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Sec-Fetch-Site': 'none',
    'Sec-Fetch-Mode': 'navigate',
    'Sec-Fetch-User': '?1',
    'Sec-Fetch-Dest': 'document',
    'Accept-Encoding': 'gzip, deflate, br',
    'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="98", "Google Chrome";v="98"',
    'sec-ch-ua-mobile': '?0'
}

def search_amazon(search_item,user_id,count=0):
    term=search_item.replace(" ","+")
    res=requests.get(f"https://www.amazon.in/s?k={term}",headers=amazon_headers)
    soup=BeautifulSoup(res.content, "html.parser")
    name_list=soup.find_all('span',{'class':'a-size-medium a-color-base a-text-normal'})
    if len(name_list)==0:
        name_list=soup.find_all('span',{'class':'a-size-base-plus a-color-base a-text-normal'})
        
    price_list=soup.find_all('span',{'class':'a-price-whole'})
    amazon_products=build_products(name_list,price_list)

    count+=1
    if(len(amazon_products)>0):
        message="Showing Amazon Lowest Price Results!!\n\n"
        for k,v in amazon_products.items():
            message+=f"- _{k}_ => ₹ *{v}*\n"
        slack_bot.send_msg(message,user_id)
    elif count<10:
       search_amazon(search_item,user_id,count)
    else:
        slack_bot.send_msg("URL FAILED",user_id)

def search_flipkart(search_item,user_id,count=0):
    term=search_item.replace(" ","%20")
    res=requests.get(f"https://www.flipkart.com/search?q={term}",headers=flipkart_headers)
    soup=BeautifulSoup(res.content, "html.parser")
    class_list=["s1Q9rs","IRpwTa"]
    name_list=soup.find_all('div',class_="_4rR01T")

    for i in range(len(class_list)):
        if len(name_list)==0:
            name_list=soup.find_all('a',class_=class_list[i])

    price_list=soup.find_all('div',class_='_30jeq3')

    fkart_products=build_products(name_list,price_list)

    count+=1
    if(len(fkart_products)>0):
        message="Showing Flipkart Lowest Price Results!!\n\n"
        for k,v in fkart_products.items():
            message+=f"- _{k}_ => ₹ *{v}*\n"
        slack_bot.send_msg(message,user_id)
    elif count<10:
        search_flipkart(search_item,user_id,count)
    else:
        slack_bot.send_msg("URL FAILED",user_id)

    
def build_products(name_list,price_list):
    products={}
    for name,price in zip(name_list,price_list):
        cost=price.text.replace(",", "").replace(".","").replace("₹","")
        item=name.text.replace(",","").replace("(","").replace(")","")
        if(products.get(item,0)> int(cost) or int(products.get(item,0))==0):
            products[item]=int(cost)
    return products

